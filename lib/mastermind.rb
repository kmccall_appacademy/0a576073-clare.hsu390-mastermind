
class Code
  attr_reader :pegs

  PEGS = {"R" => :red, "O" => :orange, "Y" => :yellow, "G" => :green, "B" => :blue, "P" => :purples}



  def self.parse(input)
    pegs = []
    input.chars.each do |letter|
      pegs << letter.upcase if PEGS.keys.include?(letter.upcase)
      raise "Error" if !PEGS.keys.include?(letter.upcase)
    end
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.shuffle[0]}
    Code.new(pegs)
  end

  def [](i)
    pegs[i]
  end


  def exact_matches(other_code)
    exact_matches = 0
    pegs.each_with_index do |color, idx|
      exact_matches += 1 if other_code[idx] == color
    end
    exact_matches
  end

  def near_matches(other_code)
    near_matches = 0
    uniq_pegs = pegs.uniq
    uniq_pegs.each_with_index do |color, idx|

      near_matches += 1 if other_code.pegs.uniq.include?(color)
    end
    near_matches - exact_matches(other_code)


end

  def ==(other_code)
    return false unless other_code.is_a? Code
    self.pegs == other_code.pegs
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    Code.parse(gets.chomp)
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    puts "You have #{exact_matches} exact matches and #{near_matches} near matches."
  end

  def play

    10.times do
      if @secret_code == get_guess
        puts "You won!"
      else
        display_matches(guess)
      end
    end
    puts "You failed!"
  end

end
